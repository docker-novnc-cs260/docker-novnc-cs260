FROM ubuntu:18.04

### 
# Begin Core NOVNC Container
###
ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root


RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update && apt-get dist-upgrade -y

RUN apt-get install -y --fix-missing --no-install-recommends \
        python-numpy \
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*


### begin noVNC  ###
WORKDIR /opt
RUN apt update -y
RUN apt install build-essential pkg-config libvncserver-dev libssl-dev xorg-dev autoconf -y
RUN wget https://github.com/LibVNC/x11vnc/archive/0.9.16.tar.gz
RUN tar -xvf 0.9.16.tar.gz
WORKDIR /opt/x11vnc-0.9.16
RUN autoreconf -fiv
RUN ./configure
RUN make
RUN make install
### end noVNC ###


RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

RUN echo "this needs to run again, delete this line whenever"
ADD startup.sh /
ADD cleanup-cruft.sh /
ADD initialize.sh /

ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/

# make sure the noVNC self.pem cert file is only readable by root
RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf
### 
# END Core NOVNC Container
###


### 
# Begin Class/Contanier Specific Installs
###
RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow
RUN  pip install future


###Liberica JDK version 12.0.2  ###
RUN wget https://download.bell-sw.com/java/12.0.2/bellsoft-jdk12.0.2-linux-amd64.deb
RUN dpkg -i bellsoft-jdk12.0.2-linux-amd64.deb


###pycharm ###
WORKDIR /opt
RUN wget https://download.jetbrains.com/python/pycharm-community-2021.3.1.tar.gz
         
RUN tar -xvf pycharm-community-2021.3.1.tar.gz
RUN rm pycharm-community-2021.3.1.tar.gz
ADD pycharm.desktop /usr/share/applications
WORKDIR /


###anaconda ###
WORKDIR /opt

RUN sudo apt-get install -y libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
RUN sudo wget https://repo.anaconda.com/archive/Anaconda3-2021.11-Linux-x86_64.sh
RUN sudo DEBIAN_FRONTEND=noninteractive bash /opt/Anaconda3-2021.11-Linux-x86_64.sh -b -p /opt/anaconda
ENV PATH="${PATH}:/opt/anaconda/bin"

ADD cs260_conda.yml .
RUN conda env create -f cs260_conda.yml
ENV PATH="/opt/anaconda/envs/cs260/env/bin:${PATH}"
RUN echo 'alias python="python3.9"' >> /home/ubuntu/.bashrc

WORKDIR /

### zip libraries ###
RUN apt-get update -y
RUN apt-get install -y zip unzip
### 
# END Class/Contanier Specific Installs
###


# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6


#https://localhost:6080/vnc.html?host=localhost&port=6080
ADD startup.sh /
ENTRYPOINT ["/startup.sh"]
